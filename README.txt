README.txt

A brief how-to for maintaining the Greenslopes webpage.

-------------------------------------------------------------

EMAIL: Greenslopes has an email account, greenslopescsu@gmail.com. If you're here, you can
easily guess the password.

WEBSITE:
Since you're here, you probably already know how to do this, but...
   * To access the files for the Greenslopes webpage, ssh into the Greenslopes account on tx1.
     On Mac/Linux this means opening a terminal and typing:
        ssh greenslo@tx1.math.colostate.edu
     You will be asked for a password which the previous administrator should have given you.
     On Windows, you will need something like PuTTY (https://putty.org/) to do this. From here
     on you are left to your own devices if you choose to use Windows.
   * It is best NOT to edit the html files live on tx1, except for very minor changes. Instead,
     use SCP to copy the directory to your local machine, edit there, then SCP the edited files
     back to the server.
     - To copy files from tx1 to your computer, open the terminal then enter
          scp greenslo@tx1.math.colostate.edu:~/public_html/filename /some/local/directory
       If you are currently in the directory you want the files saved to, use . for the local
       directory. To copy the entire directory, use * instead of filename.
     - To copy files from your computer to tx1, use 
          scp filename greenslo@tx1.math.colostate.edu:~/public_html
       (Note the lack of / at the end of the directory path.)
   * After copying files, or adding new files to tx1, you may need to set permissions on them so
     that they are publicly readable. To do this, ssh into tx1 and use
        chmod 0755 filename
   * THE BEST WAY TO EDIT THE WEBPAGE:
     Greenslopes has a BitBucket (bitbucket.org) account. This is essentially the same as
     Github. The website files are in a repository there, and that repository is set as the
     remote for public_html on tx1. If you are comfortable with git, everything that follows
     is completely standard. Otherwise, read on for instructions.
     - To edit files on tx1, login as usual and make your changes. Before logging off, commit
       those changes to git. You can run
          git status
       to see which files were modified. Then run
          git add filesto.add
       or 
          git add --all
       to add all of them to your git commit. Once you have added all the files to change, run
          git commit -m "Some sort of message"
       to make a commit, and
          git push origin master
       to send these changes off to the remote repository. You should do this every time you
       make changes! If you are asked for a password, it is the normal one.
     - Your general workflow should be to edit files locally. You can create a git repository
       locally by opening a terminal, navigating to a convenient location, then running
          git clone https://greenslopesCSU@bitbucket.org/greenslopesCSU/greenslopes_website.git
       Assuming you have git installed (all Linux and macOS machines do), this will create a 
       directory titled "greenslopes_website" in your current location with all of these files in
       it. Then you can edit as you like. When you're done editing, run the same commands as above.
     - Before making any edits, locally or on tx1, always run
          git pull origin master
       unless you definitely know what you are doing.
     - To get your local edits onto the website, just log on to tx1 and run git pull. You may also
       need to re-run chmod 0755 on any files that have been updated by a git pull.
     - Note that you will need to ssh -Y in order to successfully push commits. For some reason
       git on tx1 is configured to use a GUI popup to ask for passwords, so you need access to X.
   
The website itself is very simple. There are files title Fall20xx.html or Spring20xx.html for
most semesters Greenslopes has run. To get started with a new semester, copy the file
template2.html and rename the copy according to the current semester. Update the names of the
coordinators, the location of the seminar, the dates (both in the table at the top and in the
list of abstracts), and add a link to the previous semester's page at the bottom.

Most important: link your current semester html file to index.html. When someone on the 
internat load http://www.math.colostate.edu/~greenslo/ they are directed to index.html.
You should symlink your file (Fall20xx.html or Spring20xx.html) to index.html.
To do this, ssh into the folder on tx1 and enter
   ln -s -f yourfile.html index.html
The -s flag means to create a symbolic link and the -f is needed because there should already
be a file linked to index.html (namely the previous semester's file). Now whenever index.html
is opened, it is redirected to yourfile.html. All of your edits should be made to yourfile.html;
you never need to touch index.html directly.

Edits throughout the semester will consist of adding speaker titles and abstracts. The template
file contains samples of how these should look.

If you like, and I would recommend it, the page is set up to use MathJax to render LaTeX-style
mathematics. You should be able to use any standard command, including those in the amsmath
package. Surround inline math with \( and \). For example, \(f \colon \mathbb{R}^2 \to \R\),
which will render as expected. For longer equations you can use displayed math as in LaTeX:
just use $$ to surround it. For example:
$$
x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
$$
should render as expected.
